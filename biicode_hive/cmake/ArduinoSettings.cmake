#This file is automatically created by biicode.
#Do not modify it, as your changes will be overwritten.



# This file is a container of your list of settings that you have in your file
# settings.bii. If you have not modified this file, biicode provides you a
# default settings

# ARDUINO_DEFAULT_BOARD      - Default Arduino Board ID when not specified.
# ARDUINO_DEFAULT_PORT       - Default Arduino port when not specified.
# ARDUINO_DEFAULT_SERIAL     - Default Arduino Serial command when not specified
# ARDUINO_DEFAULT_PROGRAMMER - Default Arduino Programmer ID when not specified

set(ARDUINO_DEFAULT_BOARD uno)
set(ARDUINO_DEFAULT_PORT /dev/ttyACM0)
set(ARDUINO_DEFAULT_PROGRAMMER usbtinyisp)
set(ARDUINO_DEFAULT_SERIAL None)
