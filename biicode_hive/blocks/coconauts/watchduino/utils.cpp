/*Copyright 2014 Coconauts

This file is part of WatchDuino.

WatchDuino is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

WatchDuino is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with WatchDuino. If not, see http://www.gnu.org/licenses/.*/
#include "coconauts/watchduino/utils.h"

bool  wasPushed[13];

String getDigit(int digits)
{
  if(digits < 10)
    return "0"+String(digits);
  else
    return String(digits);
}


bool pressedButton(int pin){
    return digitalRead(pin) == LOW;
}

bool pushedButton(int pin){
    if (digitalRead(pin) == LOW){
        if (!wasPushed[pin]){
            wasPushed[pin] = true;
            return true;
        }
        return false;
    } else {
        wasPushed[pin] = false;
        return false;
    }
}

int clock12ToDeg(int clockVal) {
    if (clockVal >= 12) clockVal = clockVal-12; 
    return  90 - (30 * clockVal);
}
int clock60ToDeg(int clockVal){
    if (clockVal >= 60) clockVal = clockVal-60; 
    return  90 - (6 * clockVal);
}

int degToClockX(int degree, int r){
    return  r * cos(rad(degree));
}
int degToClockY(int degree, int r){
    return  r * sin(rad(degree));
}

float rad (int degree){
    return degree* (3.1416/180);
 }
