/*Copyright 2014 Coconauts

This file is part of WatchDuino.

WatchDuino is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

WatchDuino is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with WatchDuino. If not, see http://www.gnu.org/licenses/.*/
#ifndef WATCHDUINO_SCREEN_H 
#define WATCHDUINO_SCREEN_H

class Screen
{
    protected:
        int refresh;
    public:
        Screen(){
            refresh = 100;
        }
        int getRefresh(){
            return refresh;
        }
        virtual ~Screen(){}
        virtual void draw() = 0;
        virtual void controller() = 0;
        virtual void update() = 0;
        virtual void enter() = 0;
        virtual void exit() = 0;
};

#endif