/*Copyright 2014 Coconauts

This file is part of WatchDuino.

WatchDuino is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

WatchDuino is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with WatchDuino. If not, see http://www.gnu.org/licenses/.*/

#include <Adafruit_GFX.h> 
#include <Adafruit_PCD8544.h> 
#include <Time.h>   
#include <JeeLib.h> 
#include "notes.h"
#include "images.h"
#include "utils.h"
#include "watchcore.h"
#include <avr/pgmspace.h>

//Screens
#include "screen.h" //abstract class
#include "settings.h"
#include "alarmSet.h" 
#include "watch.h"
#include "pong.h"
#include "snake.h"
#include "timeSet.h"

//SLEEP VARIABLES
ISR(WDT_vect) { Sleepy::watchdogEvent(); }
PROGMEM prog_int16_t WAKE_PIN = 2;                 // pin used for waking up
int count = 0;                   // counter
int refresh = 1000;
const int SLEEP_TIME = 1000/refresh * 30;

//SOUND VARIABLES
#define BUZZERPIN  11
PROGMEM prog_int16_t NOTE_DURATION = 1000 / 4; //a quarter
PROGMEM prog_int16_t DELAY_BETWEEN_NOTES =1000; //ms
long SECONDS_IN_DAY = 86400;

//SCREEN VARIABLES
Adafruit_PCD8544 display = Adafruit_PCD8544(8,7,6,4,5);
int contrast = 60;
int screen = 0; 
int WIDTH= 84;
int HEIGHT= 48;

//ALARM 
bool alarm_active = 0; // 0 = inactive  
int alarm_mode = 0; // 0 = hour_alarm (ej: alarm at 5:52)
                    // 1 = remaining_alarm / timer (ej: alarm in 5 minutes)
long alarm = 0; //time in seconds
int selectedAlarm = 0;
int playingAlarm = 0;

//TIMESET
int selectedTimeSet = 0;

//SETTINGS 
bool sleep = false;
bool sound = true;
int selectedSettings = 0;
int fixTime = 0;

Screen *sys_screens[] ={ 
      new TimeSet(),
      new Watch(),
//    new Snake(),
//    new Pong(),
      new Settings(),
      new AlarmSet()
};

int sysloop(Screen *user_screens[], const int alarm_melody[], const int alarm_durations[], const int ALARM_SIZE) {

    int sysscreens_size = (sizeof(sys_screens)/sizeof(*sys_screens));
    int usescreens_size = (sizeof(user_screens)/sizeof(*user_screens));
    int screens_size = sysscreens_size + usescreens_size - 1;
    
    Screen* screens[screens_size] ;
    
    for(int i=0;i<sysscreens_size;i++) screens[i] = sys_screens[i];
    for(int i=0;i<usescreens_size;i++) screens[sysscreens_size+i] = user_screens[i];
    
    init(); // don't forget this!
    {
        //Serial.begin(9600);
        //Serial.println(freeRam());
        pinMode(WAKE_PIN, INPUT); digitalWrite(WAKE_PIN,HIGH);

        pinMode(UP_PIN, INPUT); digitalWrite(UP_PIN,HIGH);
        pinMode(DOWN_PIN, INPUT);digitalWrite(DOWN_PIN,HIGH);
        pinMode(SELECT_PIN, INPUT); digitalWrite(SELECT_PIN,HIGH);

        setTime(00,0,0,30,8,13);
        buzzer(1);

        display.begin();
        display.setContrast(contrast);

        attachInterrupt(0, wakeUp, LOW);
        refresh = 100;
    } // END INIT

    while(1) {

        display.clearDisplay();
        
        controller(screens,screens_size);
        
        checkSleep();
        printStatusBar();
                
        screens[screen]->draw();
        screens[screen]->controller();
        screens[screen]->update();

        checkAlarm(alarm_melody, alarm_durations, ALARM_SIZE);
        fixDelay();
        count++;
        display.display();
        
        if (isAlarmSounding()) Sleepy::loseSomeTime(100); 
        else Sleepy::loseSomeTime(screens[screen]->getRefresh()); 
    } //END WHILE
}

void checkSleep(){
    if (sleep){
        if(count >= 1000/refresh*SLEEP_TIME ) {
        
            printPowerDown();
    
            if (alarm_active)  Sleepy::loseSomeTime((alarm - now())*1000);
            else Sleepy::loseSomeTime(60000);
            
            display.setContrast(contrast);
            
        } else{
            count ++;
        }
    }
}


void controller(Screen *screens[], int sizeof_screens){
    if (pressedButton(SELECT_PIN) ||
        pressedButton(UP_PIN) ||
        pressedButton(DOWN_PIN)) {
        count = 0;
        if (isAlarmSounding()) alarm_active = false;
    }
    if (pressedButton(WAKE_PIN)){

        count = 0;
        screens[screen]->exit();
        screen = (screen +1) % sizeof_screens;
        screens[screen]->enter();
    }
}

void wakeUp() {
    // Just a handler for the pin interrupt.
    count = 0;
}

void fixDelay(){
    if (hour() == 0 && minute() == 0){
        adjustTime(fixTime);
    }
}

void buzzer(int times){
    for (int i = 0 ; i < times ; i++){
        tone(BUZZERPIN, NOTE_C4, NOTE_DURATION);
        if (times > 1) delay(DELAY_BETWEEN_NOTES);
    } 
}

void gameSound(){
     if (sound) {
         tone(BUZZERPIN, NOTE_C4, NOTE_DURATION);
         delay(10);
         noTone(BUZZERPIN);
     }
}
void printPowerDown(){
    display.clearDisplay();
    display.setContrast(0);
}

void checkAlarm(const int alarm_melody[], const int alarm_melody_durations[], const int ALARM_SIZE){
    if (isAlarmSounding())playAlarm(alarm_melody,alarm_melody_durations, ALARM_SIZE);
}
bool isAlarmSounding(){
    return alarm_active && alarm <= now() ;
}

void printStatusBar(){
    printBarTime();
    printBarBattery();
    if (alarm_active) printBarAlarm();
   // display.drawFastHLine(0, 7, 84, BLACK);
}
//@Deprectated
void printBarDate(){
    display.setCursor(0,0); 
    display.println(getDigit(day())+'/'+getDigit(month()));
}

void printBarTime(){
   // display.setCursor(54,0); 

    char separator = ' ';
    if (second() % 2 == 0 ) separator = ':';
    
    display.setCursor(58,0); display.println(getDigit(hour()));
    display.setCursor(68,0); display.println(separator);
    display.setCursor(72,0); display.println(getDigit(minute()));
    
    //display.println(getDigit(hour())+separator+getDigit(minute()));  
    //display.drawRoundRect(52,-1,50,10,2 , BLACK);
}

void printBarBattery(){
    //3300 is the minimum battery (bat = 0%)
    //4000 (3300 + 700) is the maximum (bat = 100%)
    //(vcc - 3300 ) * 100 / 700 = (vcc - 3300) / 7
    //We divide by 10 to get a number between 0 an 10
    int bat = (int) (readVcc() - 3300)/70;

    //battery case
    display.drawRect(41, 0, 10, 7, BLACK);
    //battery nipple
    display.drawRect(50, 1, 3, 5, BLACK);
    //fill
    display.fillRect(41, 0, min(bat,10), 7, BLACK); 
  
}
void printBarAlarm(){
    display.drawBitmap(0, 0 , getImage(SOUND),10,10, BLACK);
    //display.drawRoundRect(42,-1,11,10,2 , BLACK);
}

void playAlarm(const int alarm_melody[], const int alarm_melody_durations[], const int ALARM_SIZE){

    int noteDuration = alarm_melody_durations[playingAlarm]* 1000/8 ;
    tone(BUZZERPIN, alarm_melody[playingAlarm],noteDuration);
    delay(noteDuration * 0.6);
    noTone(BUZZERPIN);
  
    count = 0;
    if (playingAlarm == ALARM_SIZE -1 ){
        delay(1000);
        noTone(BUZZERPIN);   
        playingAlarm  = 0;
    } else {
        playingAlarm++ ;
    }
}

