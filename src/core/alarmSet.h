/*Copyright 2014 Coconauts

This file is part of WatchDuino.

WatchDuino is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

WatchDuino is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with WatchDuino. If not, see http://www.gnu.org/licenses/.*/
#pragma once

#ifndef WATCHDUINO_ALARMSET_H 
#define WATCHDUINO_ALARMSET_H

#include "screen.h"
#include "utils.h"
#include "watchcore.h"
#include <Adafruit_GFX.h>
#include <Adafruit_PCD8544.h>

class AlarmSet: public Screen {
    private: 
      void updateAlarmMode(){
        switch(alarm_mode){
            case 0: alarm = now() ;break;
            case 1: alarm = 0;  break;
        }
    }
    public:
    AlarmSet(){}
    ~AlarmSet(){}
    void draw() {

        display.drawRoundRect(20*selectedAlarm,19,18,18,2 , BLACK);
    
        //Alarm modes
        switch( alarm_mode) {
            case 0 :display.drawBitmap(0, 20 , getImage(ALARM),16,16, BLACK); break;
            case 1 :display.drawBitmap(0, 20 , getImage(TIMER),16,16, BLACK); break;
        }
    
        //print alarm time
        display.setCursor(23,25);
        display.println(getDigit(hour(alarm))+':');
        display.setCursor(43,25); 
        display.println(getDigit(minute(alarm))+':' );
        display.setCursor(63,25); 
        display.println(getDigit(second(alarm) ) );
    }
    
    void controller(){
        if (pushedButton(SELECT_PIN)) selectedAlarm = (selectedAlarm+1) %4;
        if ((pushedButton(UP_PIN) || pushedButton(DOWN_PIN)) && (selectedAlarm==0) ){
           alarm_mode = (alarm_mode +1 )%2; updateAlarmMode();
        }
        if (pressedButton(UP_PIN)) switch(selectedAlarm){
            case 1: alarm = alarm + 3600;break;
            case 2: alarm = alarm +60; break;
            case 3: alarm++; break;
        } 
        if (pressedButton(DOWN_PIN)) switch(selectedAlarm){
            case 1: alarm = (hour(alarm) <= 0)?(59*60):(alarm-3600); break;
            case 2: alarm = (minute(alarm) <= 0)?(59):(alarm-60);  break;
            case 3: alarm = (second(alarm) <= 0)?(0):(alarm-1); break;
        }   
    }
    void update(){}
    void enter() {
        if (alarm < now() && !alarm_active) updateAlarmMode() ; 
    }
    void exit() {
        switch( alarm_mode) {
            case 0 : if (alarm > now() ) alarm_active = true; break;
            case 1 : if (alarm > 0 ) {
                alarm_mode= 0 ;
                alarm_active = true; 
                alarm = now() + alarm; 
            }
            break;
        }
    }
};


#endif