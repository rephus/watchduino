/*Copyright 2014 Coconauts

This file is part of WatchDuino.

WatchDuino is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

WatchDuino is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with WatchDuino. If not, see http://www.gnu.org/licenses/.*/
#pragma once

#include "screen.h"
#include "utils.h"
#include "watchcore.h"
#include <Adafruit_GFX.h>
#include <Adafruit_PCD8544.h>

class Watch: public Screen {
    private:

    int selectedWatch;
    void drawAnalog(){
        
        int centerX = 23;
        int centerY = 24;
        int size = 23; //out circle
        
        display.drawCircle(centerX, centerY, size-2, BLACK);
        display.drawCircle(centerX, centerY,size, BLACK);

        for (int i = 0; i < 12; i++ ){
            int pointX = degToClockX(clock12ToDeg(i),size-4);
            int pointY = degToClockY(clock12ToDeg(i),size-4);
            display.drawPixel(centerX+pointX,centerY-pointY, BLACK);
        }
        
        int hourX = degToClockX(clock12ToDeg(hour()),size-10);
        int hourY = degToClockY(clock12ToDeg(hour()),size-10);
        display.drawLine(centerX, centerY, centerX+hourX, centerY-hourY, BLACK);
        
        int minX = degToClockX(clock60ToDeg(minute()),size-6);
        int minY = degToClockY(clock60ToDeg(minute()),size-6);
        display.drawLine(centerX, centerY, centerX+minX, centerY-minY, BLACK);
        
        int secX = degToClockX(clock60ToDeg(second()),size-5);
        int secY = degToClockY(clock60ToDeg(second()),size-5);
        display.drawLine(centerX, centerY, centerX+secX, centerY-secY, BLACK);
        
        display.setCursor(52,30); 
        display.println(getDigit(day())+'/'+getDigit(month()));
        
        display.setCursor(40,40); 
        display.println(dayStr(4) );
    }
    void drawWatch(){
    
        display.setCursor(0,15); 
        display.println(getDigit(day())+'/'+getDigit(month()));

        display.setCursor(40,15); 
        display.println(dayStr(4) );

        display.setCursor(0,30); 
        display.setTextSize(2);
        display.println(getDigit(hour())+':'+getDigit(minute()));

        display.setCursor(60,38); 
        display.setTextSize(1);
        display.println(':'+getDigit(second() ));
    
    }
    public:
    Watch() {
        refresh = 1000;
       selectedWatch = 0;
    }
    void draw(){
        if (selectedWatch == 0) drawWatch();
        else drawAnalog();
    }
    void controller(){
        if (pushedButton(SELECT_PIN)) selectedWatch = (selectedWatch+1) %2;
    }
    void update(){}
    void enter() {}
    void exit() {}
};
