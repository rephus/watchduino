/*Copyright 2014 Coconauts

This file is part of WatchDuino.

WatchDuino is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

WatchDuino is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with WatchDuino. If not, see http://www.gnu.org/licenses/.*/
#pragma once

#include "screen.h"
#include "utils.h"
#include "watchcore.h"
#include <Adafruit_GFX.h>
#include <Adafruit_PCD8544.h>

class Point{
    public:
        Point();
        Point(int x, int y);
        int x;
        int y;
};
Point::Point () {
  x = 0;
  y = 0;
}
Point::Point (int a, int b) {
  x = a;
  y = b;
}


class Snake: public Screen 
{
    private:
    Point head ;
    Point body [100] ;
    Point food ;
    int snakeDir ; //0 if left, 1 if up, 2 if right, 3 if down;
    int snakeScore ;
    int BODY_SIZE ;

    int random(int from, int to ){
        return (rand() % (to)) +from ;
    }
    bool collision (int x1, int y1, int x2, int y2, int m){
        return (((x1 >= x2 - m ) && (x1 <= x2 + m ) )&& 
        ((y1 >= y2 - m ) && (y1 <= y2 + m ) ));
    }
    void resetSnake(){
        for (int i = 0 ; i< BODY_SIZE; i++){
            body[i] =  Point();
        }
        head = Point(WIDTH/2,HEIGHT/2);
        snakeScore = 0;
        
        snakeDir = 0;
        
        //food =  Point(30,HEIGHT/2);
        food.x = random(2, 80);
        food.y = random(10, 34);
        //if (sound) buzzer(1);
        Sleepy::loseSomeTime(1000);
        
    }
    public:
    Snake(){
        refresh = 50;
        snakeDir = 0;
        snakeScore = 0;
        BODY_SIZE= 100;
    }
    void draw() {
    
        //playfield
        display.drawFastHLine(0, 8, 84, BLACK);
        
        //snake
        display.fillRect(head.x,head.y,3,3,BLACK);
        //body
        for (int i = 0 ; i<= snakeScore -1; i++){
            display.fillRect(body[i].x,body[i].y,3,3,BLACK);
        }
        //food
        display.fillRect(food.x,food.y,3,3,BLACK);

        //ballspeed
        //display.setCursor(0,0); 
        //display.println("Score "+String(snakeScore));
    }
    void controller(){
        //if (pushedButton(SELECT_PIN)) ;
        if (pushedButton(UP_PIN))   snakeDir = (snakeDir +1) %4;
        if (pushedButton(DOWN_PIN)) {
            snakeDir--;
            if (snakeDir <0) snakeDir = 3;
        }
    }

    void update(){
        
        //0 if left, 1 if up, 2 if right, 3 if down;
        int speed= 2;
        switch(snakeDir){
            case 0:
                head.x -= speed;
                break;
            case 1:
                head.y -= speed;
                break;
            case 2:
                head.x += speed;
                break;
            case 3:
                head.y += speed;
                break;
        }
        
        //update body
        Point lastPosition = head;
        Point tempPosition = head;
        for (int i = 0 ; i<= snakeScore -1; i++){
            //detect collision
            if (collision(head.x, head.y, body[i].x, body[i].y,0)){
                resetSnake();
                break;
            }
                
            tempPosition = body[i];
            body[i] = lastPosition;
            lastPosition = tempPosition;
        }
        if (collision(head.x,head.y,food.x,food.y,1)){
            body[snakeScore] = Point(head.x,head.y);
            snakeScore++;
            food.x = random(2, 80);
            food.y = random(10, 34);
        
            gameSound();
        }

        if ((head.x +2 > WIDTH) || (head.x < 0) ||  (head.y < 8) || (head.y +2 > HEIGHT) ) resetSnake();
    }
    void enter() {}
    void exit() {}
};
