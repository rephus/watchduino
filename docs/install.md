# Download and install

# Get a copy of the source code

First, of all, you'll need your own copy of the code, which you can retrieve with Git:

    git clone http://bitbucket.org/rephus/WatchDuino.git
    
Alternatively, we have WatchDuino in Biicode as a block. If you are a Biicode user, you can create a new hive, and then make a copy of the block into it:

    bii new watchduino
    cd watchduino
    bii open coconauts/watchduino
    
If you don't know what Git or Biicode are, you should first learn a little about them. Both require to install a client software on your box, and are operated through the command line:

- [Git reference](http://git-scm.com/documentation)
- [Biicode reference](http://docs.biicode.com/)

# Dependencies
    
You will need the Arduino SDK for your OS. Refer to [the Arduino software site](Arduino.cc/es/main/software) for downloads and instructions.

The project depends also on some external libraries that are not included in the Arduino core:

- [Adafruit_PCD8544](https://github.com/adafruit/Adafruit-PCD8544-Nokia-5110-LCD-library) - used to print text characters to the Nokia 5110 LCD screen. We require a slightly patched version of this library.
- [Adafruit_GFX](https://github.com/adafruit/Adafruit-GFX-Library) - Adafruit_GFX provides methods to print figures (lines, circles, rectangles, etc) and pixels in the screen.
- [Teensy Time](http://www.pjrc.com/teensy/td_libs_Time.html) - Provides useful functions to calculate time and dates. It returns the time in milliseconds by default as epoch.
- [JeeLib](https://github.com/jcw/jeelib) - This library provides several utility classes for Arduino. We are interested in the Sleepy class, which uses low level functions for ATMega to power off the chip and reduce its consumption to 0.

You can find these in the [`libs` folder](../libs) in your copy of the WatchDuino repository, so you don't have to worry about going hunting for them.

# Installation

They will need the dependency libraries to be reachable by your Arduino SDK, along with the [core WatchDuino library](../src/core). You have three ways to achieve this:

- Manually copy both the external libraries (under `libs`) and the core library (under `src/core`) into the `libraries` folder of your Arduino SDK installation.
- If you are running on Linux or Mac, you can run the [`install.sh` script](../install.sh), which will do exactly this, only automatically. You will need to provide the path to your Arduino installation to the script (it will prompt for it).
- If you are running Biicode, congratulations: you don't need to move a finger! The system will take care of the dependencies for you.

# Try your setup

After you've installed the libraries, you should test that you've done everything right. 

Try firing the Arduino IDE, and open the `src/watch/watch.ino` file, and then click on "verify". If you don't see errors, you have all your dependencies in place.

If you are using Biicode, go to the hive where you checked out watch and try to build:

    bii arduino:build
    
If you see no errors you're good to go.
