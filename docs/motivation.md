# Project motivation

## Proposed application originality

Our idea for Watchduino came when [the Pebble smartwatch](http://getpebble.com) came out.  For our point of view, it was the perfect watch for a hacker: programmable, power optimized, and with a perfect balance of utility and fun. Pebble made for the first time a smartwatch that made sense, since it took out of it all unnecessary complexities of a powerful hardware or a HD screen, to trade it for performance and lower costs.

We though however that the concept of Pebble could be taken even some steps further. A similar watch could be made even cheaper by using an LCD screen, or a very simple microcontroller such as and Arduino. The hack-friendly aspect could also be improved by providing not just an SDK, but the whole system as open source. 

At the moment, we found already some people that had built their own watches using Arduino. There's a special mention for [a steampunk style watch](http://www.instructables.com/id/Arduino-Watch-Build-Instructions) that while probably not being very useful for day to day wearing it looks incredibly cool.

So we thought we should give a try to building our own, our goal being to make something practical, that you could actually wear and use on a daily basis. It should be cheap, sufficiently small, and with a long battery life.

## Practical utility

Smartwatches, and wearable smart devices in general, are not really a novel concept, but it's one which the industry is just beginning to explore with. Having experienced the massive success of smartphones and tablets, the philosophy is starting to spread to other kind of devices as well, like TVs or wearables. And it wouldn't really be that surprising if in the following years it becomes to norm to attach a small inexpensive computer to everything imaginable, like toasters, or your front door's lock, or your shoes. After all, it's how all of the movies have promised The Future to be like!

The idea of adding programmable capabilities to every single thing might look superfluous at a first glance. Give people a programmable phone, and the first thing somebody will make for it is a farting app. And then a beer rating app, silly games, and things that one could argue that are not really providing any advancements for humanity. We could argue that we could focus our efforts and resources in, say, finding a cure for cancer.

This is certainly true, but the part we are missing to see is that every progress is done by small increments that build over what already exist. This is true for technology, and it's true for scientific research. There are no papers that make a brutal paradigm change to everything as we know it, but rather, thousands of superfluous ones that add little by little to each research field, and that might be used to build upon new research that is useful for humanity.

We can see superfluous technologic improvements in a similar way. Yes, you do not really need a watch with customizable alarm melodies, you don't really need a phone with games, and you don't really need shoes that lace themselves. But the technology that allows you to implement the silly features, also has the potential to be used to build useful things. As superfluous as the concept of a phone with apps or a social network to post short messages might seem, they are also massively empowering.

For hackers, makers and people who like to put their own things together it would also be a dream come true to be able to very easily being able to program and adapt everything around them.

That alone should be reason enough to feel excited about "the smartification" of everything around us. But even considering just the superfluous applications of it, what the heck! Humans are never all about productivity: we need the fun and games as well. If you think about it, we've reached a state of development and a living standard such as we don't need everyone to do mechanical work, because machines do it mostly for us. So what's left is creative work, where there's no surprise that entertainment and media have come to fill the gap.


## Commercial application, market potential

The market interest in smartwatches is relatively recent, with not so many models available at the moment. The industry however seems to be betting heavily on them, and on wearables in general, so one would expect that they would become as ubiquitous as smartphones are today.

The particular niche that WatchDuino would cover in competitive market could be, from one angle, to target the cost wise lowest end; from an even more important angle, we believe that WatchDuino offers a experience that no commercial watch is ever going to pair: openness. WatchDuino is not only programmable, it's fully hackable from hardware to software. You can build your own out of components you can buy at your local electronics store for a couple of tens of bucks, and you have the full source code of the watch's operative system at your disposal. This ability to build the whole thing from scratch, and being able to hack at every level of it will greatly appeal to electronics hobbyists and makers.

One of our plans for the future involves running a Kicstarter campaign to produce both commercial quality DIY kits and fully assembled watches, including a factory printed circuit and a fabricated case and strap.

# Technical notes

To be honest, more than the possibility of monetizing it, what really pushed us to pursue this project was the experimentation that it required, and solving the technical difficulties that we encountered along the way. WatchDuino has required a lot of iterations to reach the state in which it is now.

We started the project with the premise of building a programmable smartwatch using an Arduino. We started by programing a Nokia screen with an Arduino Nano and an Arduino Mini boards. This was not so difficult, thanks to the existence of [fantastic controller libraries for Nokia screens by Adafruit](https://github.com/adafruit/Adafruit-PCD8544-Nokia-5110-LCD-library). We then added a buzzer to produce sounds, and buttons for input.

And then we faced our first mayor problem: power consumption. All of the Arduino boards, even the mini, require around 40mA to run. This means that the 240mAh LiPo battery we were using would be depeted in just a few hours.  This was totally unacceptable, so we had to research ways of driving the prower consumption down.

We discovered that the Arduino board actually comes with a handful of components (like always-on LEDs, voltage regulators, or a USB controller) that while being very useful for development, were not really providing any useful features for our smartwatch, but were still draining precious power. Thankfully, we found a workaround for this: on the regular Arduino boards, that come with a standard microcontroller (ATMega in the case of Arduino Uno), you can extract the microcontroller from the board and use it standalone. So you only use the Arduino board itself to transfer your program to the microcontroller, but after that, you can use the chip along with a crystal oscillator and forget about the rest of the board. The power consumption is drastically reduced (going from ~40mAh to ~2mAh), making it possible, in our case, for the battery to last almost for a week.

To try to push battery life even further we implemented a sleep mode, that would power off the screen after some time of inactivity, making it save power. We also discovered a terrific library called [JeeLib](http://jeelabs.net/pub/docs/jeelib), which holds a collection of utility classes for Arduino. One of them, the [Sleepy class](http://jeelabs.net/pub/docs/jeelib/classSleepy.html) allows you sleep for a given amount of time, by consuming almost no power at all during that time (on the order of *micro Amperes*!). By using JeeLib's Sleepy for our sleep mode, the battery could potentially last for years while being asleep, and only when the watch is waken up it will get drained. With a normal usage looking at the time, and even doing sporadic game sessions, our watch was able to hold on for weeks before needing recharging.

We also found necessary to optimize the memory footprint of the software. The ATMega 328 chip we were using has 27k bytes  of flash memory for program space, and 2k bytes of SRAM for runtime. The moment you start having relatively big data structures, such as strings, arrays or bitmap images, you can run out of SRAM in no time. You can optimize your memory usage if you use the PROGMEM facility, which allows you to read pieces of data directly from the flash memory, avoiding having to load them into SRAM. The downside is that you can't modify those bits of data, so this technique is perfect for relieving the SRAM of having to hold our bitmap images, and is left exclusively for the rest of the code. There are other small code optimizations in the program, though they don't make such a dramatic impact on the memory usage. 

In the future, there's some more hardware experimentation we would like to make. For instance, we would like to see if we could integrate a bluetooth module into the system, to be able to send notifications from your smarphone (just like with the cool commercial smartwatches!).
